

## Introduction

**basic bot** is a simple Python-based, retrieval-based chatbot where the responses
are defined by "responses.txt". It is used for the purpose of the course "Implementation of a personalized chatbot",
and adapted based on the project https://github.com/parulnith/Building-a-Simple-Chatbot-in-Python-using-NLTK.
It includes:

1. **chat.py**: the basic startup code for the dialogue system.
2. **responses.txt**: the sample dialogue responses.


## The Task

* Copy the provided question-answer pairs on Teams into the **responses.txt** file and re-format them following the provided samples.
* Run the code and ensure that it is bug-free.
* **Bonus**: Modify the script to improve upon the **matching capability** (in the response() function) or any other part of the code, but leaving the **responses.txt** file intact. 


## Installation

* Clone from the Gitlab source

 ```shell
 git clone https://gitlab.com/erniecyc/basic-bot.git

 ```

* Install packages
 ```shell
 pip install -r requirements.txt

 ```


## Quickstart

* To run:

 ```
 $ python chat.py
 ```

## Development Workflow

* **Week 1**: Define response strings based on the provided text file:
 Examine the format of the "responses.txt" file and expand the response set based on its format.

<!-- * **Week 2**: Improve upon the dialogue quality:
 Interact with the chatbot and improve upon its quality by either changing
 the matching function or the "responses.txt". -->

## Other course Links

* Gitlab:
https://gitlab.com/

* Git tutorial:
https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow

* Trello board sample:
https://trello.com/b/gMzrG2Zb/künstlerisches-projekt

* Slack channel:
https://join.slack.com/t/artisticsoftw-ghc4291/shared_invite/zt-ot1tj9nr-Cd5QofYftjUitnqbd_LURQ

* Overleaf template:
https://www.overleaf.com/latex/templates/instructions-for-acl-ijcnlp-2021-proceedings/mhxffkjdwymb

* Latex tutorial:
https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes







