
#Meet Louisa: The Artist

#import necessary libraries
import io
import random
import warnings
import string # to process standard python strings
import warnings
import numpy as np
import nltk

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from nltk.stem import WordNetLemmatizer


warnings.filterwarnings('ignore')


# comment the following after the first successful run
nltk.download('popular', quiet=True) # for downloading packages
nltk.download('punkt') # first-time use only
nltk.download('wordnet') # first-time use only


#Reading in the corpus
with open('responses.txt','r', encoding='utf8', errors ='ignore') as fin:
    raw = fin.read().lower()

#Tokenisation
sent_tokens = raw.split('\n')# converts to list of sentences

# Preprocessing
lemmer = WordNetLemmatizer()
def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]
remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
def LemNormalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))


# Keyword Matching
GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up","hey",)
GREETING_RESPONSES = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]


def greeting(sentence):
    """If user's input is a greeting, return a greeting response"""
    for word in sentence.split():
        if word.lower() in GREETING_INPUTS:
            return random.choice(GREETING_RESPONSES)


def postprocess(response):
    if len(response.split('<answer>'))>1:
        return response.split('<answer>')[1]
    else:
        return response


# Generating response
def response(user_response):
    """
    This function can be modified for better matching capability.

    """
    louisa_response=''
    TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english')
    tfidf = TfidfVec.fit_transform(sent_tokens+[user_response])
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx=vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]
    if(req_tfidf == 0):
        louisa_response += "I am sorry! I don't understand you"
        return postprocess(louisa_response)
    else:
        louisa_response += sent_tokens[idx]
        return postprocess(louisa_response)


if __name__ == '__main__':
    flag=True

    # to be modified
    print("My name is Louisa. I will answer your queries. If you want to exit, type Bye!")
    while(flag==True):

        # get user response
        user_response = input()
        user_response=user_response.lower()

        # retrieving bot response
        if(user_response!='bye'):
            if user_response=='thanks' or user_response=='thank you':
                flag=False
                print("You are welcome..")
            else:
                if(greeting(user_response)!=None):
                    print(str(greeting(user_response)) )
                else:
                    print("",end="")
                    print(response(user_response))
        else:
            flag=False
            print("Bye! take care..")
